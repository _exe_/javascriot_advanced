const books = [{
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

const root = document.createElement("div");
const ul = document.createElement("ul");
root.id = "root";
root.append(ul);
document.body.prepend(root);


const sortedBooks = [];
books.forEach(element => {
  const {
    author,
    name,
    price
  } = element;

  try {
    if (author === undefined) {
      throw new Error("author is undefined");
    } else if (price === undefined) {
      throw new Error("price is undefined");
    } else if (name === undefined) {
      throw new Error("name is undefined");
    } else {
      sortedBooks.push(element);
    }
  } catch (error) {
    console.log(error);
  }
});


sortedBooks.forEach(item => {
  const li = document.createElement("li");
  const booksEl = document.createElement("ul");
  const bookData = `
    <li>${item.author}</li>
    <li>${item.name}</li>
    <li>${item.price}</li>
  `;
  booksEl.innerHTML = bookData;
  li.append(booksEl);
  ul.append(li);
});