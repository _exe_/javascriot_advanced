handleData("https://swapi.dev/api/films/");

async function sendRequest(URL) {
  const res = await fetch(URL);
  const data = await res.json();
  return data;
}

async function handleData(URL) {
  const data = await sendRequest(URL);
  const films = await getFilms(data);
  await renderFilm(films);
}

async function getFilms(films) {
  const filmsData = await films.results.map(element => {
    const actors = []
    const fs = async () => {
      for await (const iterator of element.characters) {
        const name = await sendRequest(iterator)
        actors.push(name.name)
      }
    }
    fs()

    return {
      actors: actors,
      episode_id: element.episode_id,
      title: element.title,
      opening_crawl: element.opening_crawl
    };
  });
  const allData = await Promise.all(filmsData)
  return allData
}



async function renderFilm(data) {
  // console.log(data)
  const ul = document.querySelector("#list");

  await data.forEach(async element => {
    console.log(element)
    const li = document.createElement("li");
    const actorList = await element.actors.map(item => {
      return `<ul>
        <li>${item}</li>
      </ul>`
    })
    li.innerHTML = `
    <ul>
      <li><b>Episode id</b>: ${element.episode_id}</li>
      <li><b>Title:</b>  ${element.title}</li>
      <li><b>Opening crawl:</b>  ${element.opening_crawl}</li>
      <li><b>ActorList:</b> ${actorList}</li></li>
    </ul>
    `;
    ul.appendChild(li);
  });
}