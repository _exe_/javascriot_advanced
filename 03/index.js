class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  set name(value) {
    return this._name = value;
  }
  get name() {
    return this._name;
  }
  set age(value) {
    return this._age = value;
  }
  get age() {
    return this._age;
  }
  set salary(value) {
    return this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(...args) {
    super(...args)
    const [, , , lang] = args;
    this.lang = lang;
  }
  get salary() {
    return this._salary *= 3;
  }
}

const se = new Programmer("Vasya", 25, 35000, ["java", "sql"]);
const se2 = new Programmer("Kolya", 19, 21000, ["js", "node.js"]);
const se3 = new Programmer("Andrew", 45, 55000, ["java", "sql", "c++", "golang"]);

console.log(se);
console.log(se2);
console.log(se3);