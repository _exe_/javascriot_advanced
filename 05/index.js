const btn = document.querySelector("#findIp");
btn.addEventListener("click", handleIp)

function sendRequest(url) {
  return fetch(url);
}

async function handleIp() {
  try {
    const res = await sendRequest("https://api.ipify.org/?format=json");
    if (res.status === 200) {
      const ip = await res.json();
      await getApi(ip)
    } else {
      throw new Error("Func handleIp: Status is not 200")
    }
  } catch (error) {
    console.error(error)
  }
}

async function getApi(data) {
  try {
    const res = await sendRequest(`http://ip-api.com/json/${data.ip}?fields=1572895`);
    if (res.status === 200) {
      const apiInfo = await res.json();
      await showData(apiInfo);
    } else {
      throw new Error("Func getApi: Status is not 200")
    }
  } catch (error) {
    console.error(error)
  }
}

function showData(apiInfo) {
  const data = document.createElement("ul");
  data.innerHTML = `
  <li><b>continent:</b> ${apiInfo.continent}</li>
  <li><b>country: </b>${apiInfo.country}</li>
  <li><b>countryCode:</b> ${apiInfo.countryCode}</li>
  <li><b>region: </b>${apiInfo.region}</li>
  <li><b>regionName: </b>${apiInfo.regionName}</li>
  <li><b>city: </b>${apiInfo.city}</li>
  <li><b>district:</b> ${apiInfo.district || "District not found"}</li>
  `;
  btn.after(data);
}